import { ADD_MESSAGE, CLEAR_MESSAGES, REMOVE_MESSAGE } from './Actions/MessagesAction';

const initState = {
  messages: [],
};

function messagesReducer(state = initState, action = {}) {
  switch (action.type) {
    case ADD_MESSAGE:
      return {
        ...state,
        messages: [...state.messages, action.payload],
      };
    case REMOVE_MESSAGE:
      return {
        ...state,
        messages: state.messages.filter((message) => message !== action.payload),
      };
    case CLEAR_MESSAGES:
      return {
        ...state,
        messages: action.payload,
      };
    default:
      return state;
  }
}

export default messagesReducer;
