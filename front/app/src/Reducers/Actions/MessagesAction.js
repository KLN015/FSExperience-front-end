export const ADD_MESSAGE = 'ADD_MESSAGE';
export const REMOVE_MESSAGE = 'REMOVE_MESSAGE';
export const CLEAR_MESSAGES = 'CLEAR_MESSAGES';

export const addMessage = (message) => (dispatch) => {
  dispatch({
    type: ADD_MESSAGE,
    payload: message,
  });
};

export const removeMessage = (message) => (dispatch) => {
  dispatch({
    type: REMOVE_MESSAGE,
    payload: message,
  });
};

export const clearMessages = () => (dispatch) => {
  dispatch({
    type: CLEAR_MESSAGES,
    payload: [],
  });
};
