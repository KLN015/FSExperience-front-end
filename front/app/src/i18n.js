/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable quotes */
import i18next from "i18next";
import HttpBackend from "i18next-http-backend";
import LanguageDetector from "i18next-browser-languagedetector";
import { initReactI18next } from "react-i18next";

const apiKey = 'eXqS9Gx8zxPODzBa_nAxng';
const loadPath = `https://api.i18nexus.com/project_resources/translations/{{lng}}/{{ns}}.json?api_key=${apiKey}`;

i18next
  .use(HttpBackend)
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    fallbackLng: "en",

    ns: ["default"],
    defaultNS: "default",

    // eslint-disable-next-line comma-spacing
    supportedLngs: ["en","ca","es","fr"],
    backend: {
      // eslint-disable-next-line object-shorthand, comma-dangle
      loadPath: loadPath
    // eslint-disable-next-line comma-dangle
    }
  // eslint-disable-next-line semi
  })
