import {
  Button,
  Divider, Modal, Stack, TextField,
} from '@mui/material';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import propTypes from 'prop-types';
import React, { useRef } from 'react';
import { toast } from 'react-toastify';
import CardPost from './CardPost';
import { API_URL } from '../constants';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
  borderRadius: 3,
};

function HeaderChat({ author }) {
  return (
    <div
      style={{
        padding: '20px 30px 10px 30px',
        top: 0,
        zIndex: 1,
        position: 'absolute',
        backgroundColor: '#141414',
        width: '100%',
      }}
    >
      <Stack
        direction="column"
      >
        <Stack
          direction="row"
          alignItems="center"
          sx={{ marginBottom: (2) }}
        >
          <Avatar
            sx={{
              width: 40,
              height: 40,
              marginRight: (2),
            }}
          />
          <Typography variant="h6">
            {author}
          </Typography>
        </Stack>
        <Divider />
      </Stack>

    </div>
  );
}

HeaderChat.propTypes = {
  author: propTypes.string.isRequired,
};

function ModalNewPost({ open, handleClose, setPosts }) {
  const getPosts = async () => {
    const token = localStorage.getItem('token');
    const response = await fetch(`${API_URL}/api/messages`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    });
    const data = await response.json();
    setPosts(data);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const formData = new FormData(event.currentTarget);
    const title = formData.get('title');
    const content = formData.get('content');

    try {
      const token = localStorage.getItem('token');
      const response = await fetch(`${API_URL}/api/messages/new`, {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ title, content }),
      });

      if (!response.ok) {
        const errorData = await response.json();

        throw errorData.error;
      }
      getPosts();
    } catch (error) {
      toast.error(error, {
        position: 'top-center',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'colored',
      });
    }
  };

  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box component="form" onSubmit={handleSubmit} noValidate sx={style}>

        <Typography id="modal-modal-title" variant="h6" component="h2">
          New Post
        </Typography>

        <Typography id="modal-modal-description" sx={{ mt: 2 }}>
          <div style={{ padding: 4, paddingBottom: 10 }}>
            <TextField
              label="Title"
              variant="outlined"
              fullWidth
              required
              id="title"
              name="title"
              autoFocus
            />
          </div>

          <div style={{ padding: 4, paddingBottom: 10 }}>
            <TextField
              required
              name="content"
              type="content"
              id="content"
              label="Content"
              multiline
              rows={4}
              fullWidth
            />
          </div>
        </Typography>

        <Button
          variant="contained"
          color="neutral"
          fullWidth
          type="submit"
        >
          Create New Post
        </Button>

      </Box>
    </Modal>
  );
}

ModalNewPost.propTypes = {
  open: propTypes.bool.isRequired,
  handleClose: propTypes.func.isRequired,
  setPosts: propTypes.func.isRequired,
};

function News() {
  const chatEndRef = useRef(null);
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const [posts, setPosts] = React.useState([]);

  const getPost = async () => {
    const token = localStorage.getItem('token');
    const response = await fetch(`${API_URL}/api/messages`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    });
    const data = await response.json();

    const filtredData = data.map((post) => {
      const date = new Date(post.createdAt);
      const day = date.getDate();
      const month = date.getMonth() + 1;
      const year = date.getFullYear();
      const newDate = `${day}/${month}/${year}`;
      const userId = localStorage.getItem('id');
      const likeObj = post.Users.map(
        (user) => (parseInt(user.Like.userId, 10) === parseInt(userId, 10)
          && user.Like.isLiked === 1),
      ) || null;
      const updatedPost = {
        ...post,
        date: newDate,
        isLike: likeObj[0],
      };

      return updatedPost;
    });

    filtredData.sort((a, b) => a.id - b.id);
    setPosts(filtredData);
  };

  React.useEffect(() => {
    getPost();
  }, []);

  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-end',
        height: '100vh',
        borderLeft: 1,
        borderRight: 1,
        borderColor: 'grey.800',
        backgroundColor: '#141414',
        position: 'relative',
      }}
    >
      <HeaderChat author="Home - Create Post" />
      <ModalNewPost open={open} handleClose={handleClose} setPosts={setPosts} />
      <div style={{
        overflow: 'auto',
        overflowY: 'scroll',
        overflowX: 'hidden',
        zIndex: 0,
        paddingLeft: 30,
        paddingRight: 30,
      }}
      >
        {posts && posts.map((post, index) => {
          const key = `post-${index}`;
          return (
            <div style={{ padding: 4 }} key={key}>
              <CardPost post={post} />
            </div>
          );
        })}

        <div ref={chatEndRef} />
        <div style={{ padding: 4, paddingBottom: 10 }}>
          <Button variant="contained" color="neutral" fullWidth onClick={handleOpen}>
            Create New Post
          </Button>
        </div>
      </div>

    </Box>
  );
}

export default News;
