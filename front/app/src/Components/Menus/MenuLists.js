import { Avatar, Stack, Typography } from '@mui/material';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Divider from '@mui/material/Divider';
import { useTheme } from '@mui/styles';
import PropTypes from 'prop-types';
import React from 'react';
import PowerSettingsNewIcon from '@mui/icons-material/PowerSettingsNew';
import { useDispatch } from 'react-redux';
import MessageGroupItem from './MessageGroupItem';
import { setUserLoggedOut } from '../../Reducers/Actions/AuthAction';

function MenuButtons({ handleMenuItemClick }) {
  return (
    <Stack
      direction="row"
      justifyContent="space-between"
      alignItems="center"
      spacing={1}
      pt={1}
    >

      <Button
        variant="contained"
        color="neutral"
        sx={{ fontSize: 10 }}
        onClick={() => handleMenuItemClick(0)}
        fullWidth
      >
        Home
      </Button>

    </Stack>
  );
}

MenuButtons.propTypes = {
  handleMenuItemClick: PropTypes.func.isRequired,
};

export default function MenuLists({ setSelectedIndex, setSelectedIListItem }) {
  const dispatch = useDispatch();
  const username = localStorage.getItem('username');
  const theme = useTheme();

  const handleMenuItemClick = (index) => {
    setSelectedIndex(index);
    setSelectedIListItem(0);
  };

  return (
    <Box
      sx={{
        height: '100vh',
        backgroundColor: theme.palette.background.paper,
        borderLeft: 1,
        borderColor: 'grey.800',
      }}
    >
      <div style={{ padding: 15 }}>
        <Stack direction="column" alignItems="center" justifyContent="center" spacing={1}>
          <Avatar
            alt="Avatar"
            sx={{ width: 56, height: 56 }}
          />
          <Stack direction="row" alignItems="center" spacing={1}>
            <Typography variant="h6">
              {username}
            </Typography>
            <PowerSettingsNewIcon
              sx={{ fontSize: 15 }}
              onClick={() => dispatch(setUserLoggedOut())}
            />
          </Stack>

        </Stack>

        <Divider sx={{ padding: 0.5 }} />
        <MenuButtons handleMenuItemClick={handleMenuItemClick} />
        <MessageGroupItem setSelectedIListItem={setSelectedIListItem} />
      </div>
    </Box>
  );
}

MenuLists.propTypes = {
  setSelectedIndex: PropTypes.func.isRequired,
  setSelectedIListItem: PropTypes.func.isRequired,
};
