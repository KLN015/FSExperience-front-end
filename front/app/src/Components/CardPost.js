import ThumbDownIcon from '@mui/icons-material/ThumbDown';
import ThumbDownOffAltIcon from '@mui/icons-material/ThumbDownOffAlt';
import ThumbUpAltIcon from '@mui/icons-material/ThumbUpAlt';
import ThumbUpOffAltIcon from '@mui/icons-material/ThumbUpOffAlt';
import { Card, CardActions, CardContent } from '@mui/material';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import React from 'react';
import propTypes from 'prop-types';
import { toast } from 'react-toastify';
import { API_URL } from '../constants';

function CardPost({ post }) {
  const [like, setLike] = React.useState(post.isLike);

  const handleDislike = async () => {
    setLike(false);
    try {
      const token = localStorage.getItem('token');
      const response = await fetch(`${API_URL}/api/messages/${post.id}/vote/dislike`, {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      if (!response.ok) {
        const errorData = await response.json();

        throw errorData.error;
      }
    } catch (error) {
      toast.error(error, {
        position: 'top-center',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'colored',
      });
    }
  };

  const handleLike = async () => {
    setLike(true);
    try {
      const token = localStorage.getItem('token');
      const response = await fetch(`${API_URL}/api/messages/${post.id}/vote/like`, {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      if (!response.ok) {
        const errorData = await response.json();

        throw errorData.error;
      }
    } catch (error) {
      toast.error(error, {
        position: 'top-center',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'colored',
      });
    }
  };

  return (
    <Card sx={{ minWidth: 275 }}>

      <CardContent>
        <Typography variant="h5" component="div">
          {post.title}
        </Typography>
        <Typography variant="body2">
          {post.content}
        </Typography>
      </CardContent>

      <CardActions>
        <IconButton aria-label="add to favorites" onClick={handleLike}>
          {like ? <ThumbUpAltIcon />
            : <ThumbUpOffAltIcon />}
        </IconButton>
        <IconButton aria-label="add to favorites" onClick={handleDislike}>
          {like === false ? <ThumbDownIcon />
            : <ThumbDownOffAltIcon />}
        </IconButton>
      </CardActions>

    </Card>

  );
}

CardPost.propTypes = {
  post: propTypes.shape({
    id: propTypes.number.isRequired,
    title: propTypes.string.isRequired,
    content: propTypes.string.isRequired,
    likes: propTypes.number.isRequired,
    isLike: propTypes.bool,
  }).isRequired,
};

export default CardPost;
