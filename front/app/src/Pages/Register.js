import * as React from 'react';
import { useState } from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import { setUserLoggedIn } from '../Reducers/Actions/AuthAction';
import { API_URL } from '../constants';

const languages = [
  { value: '', text: 'Options' },
  { value: 'en', text: 'English' },
  { value: 'fr', text: 'French' },
  { value: 'es', text: 'Spanish' },
  { value: 'ca', text: 'Catalan' },
];

function getMessageText(message, t) {
  if (message && message.text && message.text.includes('email is not valid')) {
    return (`${t('email.notvalid')}`);
  }
  if (message && message.text && message.text.includes('password invalid')) {
    return (`${t('email.notvalid')}`);
  }
  if (message && message.text && message.text.includes('wrong username')) {
    return t('wrong.username');
  }
  return message && message.text ? message.text : '';
}

export default function SignUp() {
  const dispatch = useDispatch();
  const { t, i18n } = useTranslation();
  const [selectedLang, setSelectedLang] = useState(i18n.language || 'en');
  const handleChange = (e) => {
    const langValue = e.target.value;
    setSelectedLang(langValue);
    i18n.changeLanguage(langValue);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const formData = new FormData(event.currentTarget);
    const email = formData.get('email');
    const username = formData.get('firstName');
    const password = formData.get('password');

    try {
      const response = await fetch(`${API_URL}/api/users/register`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, username, password }),
      });

      if (!response.ok) {
        const errorData = await response.json();

        throw errorData.error;
      }
      const data = await response.json();
      const meResponse = await fetch(`${API_URL}/api/users/me`, {
        headers: {
          Authorization: `Bearer ${data.token}`,
        },
      });

      if (!meResponse.ok) {
        const errorData = await meResponse.json();

        throw getMessageText(errorData.error, t);
      }

      const meData = await meResponse.json();
      localStorage.setItem('username', meData.username);
      dispatch(setUserLoggedIn());
    } catch (error) {
      toast.error(error, {
        position: 'top-center',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'colored',
      });
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Avatar sx={{ m: 1 }}>
          <LockOutlinedIcon />
        </Avatar>
        <div>
          <select value={selectedLang} onChange={handleChange}>
            {languages.map((item) => (
              <option
                key={item.value}
                value={item.value}
              >
                {item.text}
              </option>
            ))}
          </select>
        </div>
        <Typography component="h1" variant="h5">
          {t('translation.sign_up')}
        </Typography>
        <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="given-name"
                name="firstName"
                required
                fullWidth
                id="firstName"
                label={t('translation.first_name')}
                autoFocus
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                fullWidth
                id="lastName"
                label={t('translation.last_name')}
                name="lastName"
                autoComplete="family-name"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                id="email"
                label={t('translation.email_address')}
                name="email"
                autoComplete="email"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                name="password"
                label={t('translation.password')}
                type="password"
                id="password"
                autoComplete="new-password"
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="neutral"
            sx={{ mt: 3, mb: 2 }}
          >
            {t('translation.register')}
          </Button>
          <Grid container justifyContent="flex-end">
            <Grid item>
              <Link href="login" variant="body2">
                {t('translation.already_have_account')}
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Container>
  );
}
