import * as React from 'react';
import { useState } from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import { setUserLoggedIn } from '../Reducers/Actions/AuthAction';
import { API_URL } from '../constants';

const languages = [
  { value: '', text: 'Options' },
  { value: 'en', text: 'English' },
  { value: 'fr', text: 'French' },
  { value: 'es', text: 'Spanish' },
  { value: 'ca', text: 'Catalan' },
];

function getMessageText(message, t) {
  if (message && message.text && message.text.includes('email is not valid')) {
    return (`${t('email.notvalid')}`);
  }
  if (message && message.text && message.text.includes('password invalid')) {
    return (`${t('email.notvalid')}`);
  }
  if (message && message.text && message.text.includes('wrong username')) {
    return t('wrong.username');
  }
  return message && message.text ? message.text : '';
}

export default function Login() {
  const { t, i18n } = useTranslation();
  const [selectedLang, setSelectedLang] = useState(i18n.language || 'en');
  const handleChange = (e) => {
    const langValue = e.target.value;
    setSelectedLang(langValue);
    i18n.changeLanguage(langValue);
  };
  const dispatch = useDispatch();
  const handleSubmit = async (event) => {
    event.preventDefault();
    const formData = new FormData(event.currentTarget);
    const email = formData.get('email');
    const password = formData.get('password');

    try {
      const response = await fetch(`${API_URL}/api/users/login`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, password }),
      });

      if (!response.ok) {
        const errorData = await response.json();

        throw errorData.error;
      }
      const data = await response.json();
      localStorage.setItem('token', data.token);
      const meResponse = await fetch(`${API_URL}/api/users/me`, {
        headers: {
          Authorization: `Bearer ${data.token}`,
        },
      });

      if (!meResponse.ok) {
        const errorData = await meResponse.json();

        throw getMessageText(errorData.error, t);
      }

      const meData = await meResponse.json();
      localStorage.setItem('username', meData.username);
      localStorage.setItem('id', meData.id);
      dispatch(setUserLoggedIn());
    } catch (error) {
      toast.error(error, {
        position: 'top-center',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'colored',
      });
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
          <LockOutlinedIcon />
        </Avatar>
        <div>
          <select value={selectedLang} onChange={handleChange}>
            {languages.map((item) => (
              <option
                key={item.value}
                value={item.value}
              >
                {item.text}
              </option>
            ))}
          </select>
        </div>
        <Typography component="h1" variant="h5">
          {t('login.signIn')}
        </Typography>
        <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
          <TextField
            margin="normal"
            required
            fullWidth
            id="email"
            label={t('emailadress')}
            name="email"
            autoComplete="email"
            autoFocus
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="password"
            label={t('password')}
            type="password"
            id="password"
            autoComplete="current-password"
          />
          <Button
            type="submit"
            fullWidth
            color="neutral"
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            {t('login.loginButton')}
          </Button>
          <Grid container>
            <Grid item xs>
              <Link href="#/" variant="body2">
                {t('login.forgotPassword')}
              </Link>
            </Grid>
            <Grid item>
              <Link href="register" variant="body2">
                {t('login.noAccount')}
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Container>
  );
}
