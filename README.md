# FSExperience Front End

### Full Stack JAVASCRIPT Deployment Guide

Hello!

#### Prerequisites

Firstly, you will need to install Docker in order to deploy our Full Stack application. Please make sure to have Node.js installed on this machine as well.

https://docs.docker.com/engine/install/

https://nodejs.org/en/.

Next, you need to make sure that you have all the necessary Node.js modules to run your application. You can install them by running the command npm install.

#### Steps

Inside of docker-compose.yml change the path of the build context of react from ./front to your own path

`docker-compose up -d --build`

#### Optional

`cd front/app/`
`npm install`
`npm start`

Google cloud site:

http://34.175.199.16/